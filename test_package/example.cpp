#include <iostream>
#include "xlnt/xlnt.hpp"

int main() {
    auto wb = xlnt::workbook{};
    auto ws = wb.active_sheet();

    ws.rows(false);

    return 0;
}
