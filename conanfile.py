from conans import ConanFile, CMake, tools
import shutil

class XlntConan(ConanFile):
    name        = "xlnt"
    version     = "1.5.0"
    license     = "MIT"
    author      = "toge.mail@gmail.com"
    url         = "https://bitbucket.org/toge/conan-xlnt"
    description = "Cross-platform user-friendly xlsx library for C++11+"
    topics      = ("cross-platform", "xlsx")
    settings    = "os", "compiler", "build_type", "arch"
    generators  = "cmake"
    requires    = ("miniz/[>=2.1.0]@toge/stable", "utfcpp/[>=3.1]@toge/stable", )

    options = {
        "shared": [True, False],
    }
    default_options = {
        "shared": False,
    }

    def source(self):
        tools.get("https://github.com/tfussell/xlnt/archive/v{}.zip".format(self.version))
        shutil.move("xlnt-{}".format(self.version), "xlnt")
        
        # TODO: make libstudxml required recipe
        tools.replace_in_file("xlnt/source/CMakeLists.txt", "file(GLOB MINIZ_HEADERS ${THIRD_PARTY_DIR}/miniz/*.h)", "")
        tools.replace_in_file("xlnt/source/CMakeLists.txt", "file(GLOB MINIZ_SOURCES ${THIRD_PARTY_DIR}/miniz/*.c)", "")
        tools.replace_in_file("xlnt/source/CMakeLists.txt", "${XLNT_SOURCE_DIR}/../third-party/miniz", "")
        tools.replace_in_file("xlnt/source/CMakeLists.txt", "${XLNT_SOURCE_DIR}/../third-party/utfcpp)", ")")

        tools.replace_in_file("xlnt/CMakeLists.txt", "project(xlnt_all)", '''project(xlnt_all)
include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()''' )

    def build(self):
        cmake = CMake(self)

        cmake.definitions["STATIC"]     = not self.options.shared
        cmake.definitions["TESTS"]      = False
        cmake.definitions["BENCHMARKS"] = False
        # TODO: pybind11
        cmake.definitions["PYTHON"]     = False
        cmake.definitions["STATIC_CRT"] = False
        cmake.definitions["COVERAGE"]   = False

        cmake.configure(source_folder="xlnt")
        cmake.build()

    def package(self):
        self.copy("*.hpp",   dst="include", src="xlnt/include")
        self.copy("*.lib",   dst="lib",     keep_path=False)
        self.copy("*.dll",   dst="bin",     keep_path=False)
        self.copy("*.so",    dst="lib",     keep_path=False)
        self.copy("*.dylib", dst="lib",     keep_path=False)
        self.copy("*.a",     dst="lib",     keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["xlnt"]
